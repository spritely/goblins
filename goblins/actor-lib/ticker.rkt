#lang racket

;;; Copyright 2019-2020 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;; Add a "ticking" object, where objects can register to be ticked
;; eg on a game loop.  If the called 'tick method responds with
;; 'die the actor will not be queued for ticking again.

(require "../core.rkt"
         "cell.rkt"
         racket/match)

(provide spawn-ticker-pair)

(define (spawn-ticker-pair)
  (define-cell new-ticked
    '())
  ;; This registers new ticked objects
  (define ((^tick-register bcom) . entries)
    ($ new-ticked (append entries ($ new-ticked))))
  ;; This runs all ticked objects
  (define (^ticker bcom current-ticked)
    (make-keyword-procedure
     (lambda (kws kw-args . args)
       ;; Update set of tickers with any that have been
       ;; added since when we last ran
       (define updated-ticked
         (append ($ new-ticked) current-ticked))
       ;; reset new-ticked
       ($ new-ticked '())
       ;; Now run all ticked objects
       (define next-tickers
         (foldr (lambda (tick-me next-queue)
                  (match (keyword-apply $ kws kw-args tick-me args)
                    ['die next-queue]
                    [_ (cons tick-me next-queue)]))
                '()
                updated-ticked))
       ;; update ourself
       (bcom (^ticker bcom next-tickers)))))
  (list (spawn ^tick-register)
        (spawn ^ticker '())))

(module+ test
  (require rackunit)

  (define am (make-actormap))
  (match-define (list register-ticker ticker-tick)
    (actormap-run! am spawn-ticker-pair))
  (define joe-speaks-here
    (actormap-spawn! am ^cell))
  (define jane-speaks-here
    (actormap-spawn! am ^cell))
  (define (^malaise-sufferer bcom name speaking-cell
                             [maximum-suffering 3])
    (define ((loop n))
      (if (> n maximum-suffering)
          (begin
            ($ speaking-cell
               (format "<~a> you know what? I'm done."
                       name))
            'die)
          (begin
            ($ speaking-cell
               (format "<~a> sigh number ~a"
                       name n))
            (bcom (loop (add1 n))))))
    (loop 1))
  (define joe
    (actormap-spawn! am ^malaise-sufferer "joe"
                     joe-speaks-here))
  (define jane
    (actormap-spawn! am ^malaise-sufferer "jane"
                     jane-speaks-here
                     2))
  (actormap-poke! am register-ticker joe jane)
  (actormap-poke! am ticker-tick)
  (check-equal?
   (actormap-peek am joe-speaks-here)
   "<joe> sigh number 1")
  (check-equal?
   (actormap-peek am jane-speaks-here)
   "<jane> sigh number 1")

  (actormap-poke! am ticker-tick)
  (check-equal?
   (actormap-peek am joe-speaks-here)
   "<joe> sigh number 2")
  (check-equal?
   (actormap-peek am jane-speaks-here)
   "<jane> sigh number 2")

  (actormap-poke! am ticker-tick)
  (check-equal?
   (actormap-peek am joe-speaks-here)
   "<joe> sigh number 3")
  (check-equal?
   (actormap-peek am jane-speaks-here)
   "<jane> you know what? I'm done.")

  (actormap-poke! am ticker-tick)
  (check-equal?
   (actormap-peek am joe-speaks-here)
   "<joe> you know what? I'm done.")
  (check-equal?
   (actormap-peek am jane-speaks-here)
   "<jane> you know what? I'm done."))
