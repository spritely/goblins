#lang racket/base

;;; Copyright 2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;; sync/pr and sync/pr* produce promises that are fulfilled upon the
;; completion of a synchronizable event.
;; They set up a separate thread which watches for synchronization.

(require "../core.rkt"
         (submod "../core.rkt" np-extern)
         racket/match)

(provide sync/pr* sync/pr)

;; This one is also cancelable, but it returns two values to its continuation.
;; It only accepts one argument, so you'll have to choice-evt yourself if need be.
(define (sync/pr* evt #:timeout [timeout #f])
  (define-values (promise resolver)
    (spawn-promise-values))
  (define cancel-semaphore (make-semaphore))
  (define (cancel!)
    (semaphore-post cancel-semaphore))

  (syscaller-free-thread
   (lambda ()
     (sync (handle-evt evt
                       (lambda result-args
                         (<-np-extern resolver 'fulfill result-args)))
           (handle-evt cancel-semaphore
                       (lambda _
                         (<-np-extern resolver 'break 'canceled)))
           (if timeout
               (handle-evt (alarm-evt (+ (current-inexact-milliseconds)
                                         (* timeout 1000))))
               never-evt))))
  (values promise cancel!))

;; No way to cancel, just returns the promise
(define (sync/pr #:timeout [timeout #f] . evts)
  (match evts
    [(list evt)
     (define-values (promise _cancel)
       (sync/pr* evt #:timeout timeout))
     promise]
    [evts
     (define-values (promise _cancel)
       (sync/pr* (apply choice-evt evts) #:timeout timeout))
     promise]))

(module+ test
  (require rackunit
           "../vat.rkt"
           "bootstrap.rkt")
  (define-vat-run vat-run (make-vat))

  (let ([result-ch (make-channel)]
        [incoming-ch (make-channel)])
    (vat-run
     (on (sync/pr incoming-ch)
         (lambda (val)
           (channel-put result-ch `(success ,val)))
         #:catch
         (lambda (err)
           (channel-put result-ch `(failure ,err)))))
    (channel-put incoming-ch 'hello-there)
    (test-equal?
     "sync/pr simple success case"
     (sync/timeout 1 result-ch)
     '(success (hello-there))))

  (let ([result-ch (make-channel)]
        [incoming-ch (make-channel)])
    (define cancel
      (vat-run
       (define-values (p cancel)
         (sync/pr* incoming-ch))
       (on p
           (lambda (val)
             (channel-put result-ch `(success ,val)))
           #:catch
           (lambda (err)
             (channel-put result-ch `(failure ,err))))
       cancel))
    (cancel)
    (test-equal?
     "sync/pr* canceled"
     (sync/timeout 1 result-ch)
     '(failure canceled))))
