#lang info

;;; Copyright 2019-2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define deps '("base" "crypto" "syrup" "pk"))
(define build-deps '("rackunit-lib"
                     "scribble-lib" "sandbox-lib"
                     "racket-doc"))
(define pkg-desc
  "A transactional, distributed actor model environment")
(define version "0.12.0")
(define pkg-authors '("cwebber"))
(define scribblings '(("scribblings/goblins.scrbl" (multi-page))))
(define license 'Apache-2.0)
